package com.witoldw.calcs;

public abstract class Credit {
	
	protected double capital;  //kwota
	protected int issueOfInstallment;  // liczba rat
	protected int installmentNumber; // numer raty
	protected double rateOfInterest;  // oprocetmnowanie
	protected double standingCharge;  // oplata stala
	protected double capitalInstallment;
		
	public Credit(double capital, int issueOfInstallment, double rateOfInterest, double standingCharge) {
		super();
		this.capital = capital;
		this.issueOfInstallment = issueOfInstallment;
		this.rateOfInterest = rateOfInterest;
		this.standingCharge = standingCharge;
		this.capitalInstallment = this.capital / (double) this.issueOfInstallment;
	}
	
	public double getCapital() {
		return capital;
	}
	public void setCapital(double capital) {
		this.capital = capital;
	}
	
	
	
	public int getInstallmentNumber() {
		return installmentNumber;
	}

	public void setInstallmentNumber(int installmentNumber) {
		this.installmentNumber = installmentNumber;
	}

	public int getIssueOfInstallment() {
		return issueOfInstallment;
	}
	public void setIssueOfInstallment(int issueOfInstallment) {
		this.issueOfInstallment = issueOfInstallment;
	}
	public double getRateOfInterest() {
		return rateOfInterest;
	}
	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}
	public double getStandingCharge() {
		return standingCharge;
	}
	public void setStandingCharge(double standingCharge) {
		this.standingCharge = standingCharge;
	}
	
	
	
}
