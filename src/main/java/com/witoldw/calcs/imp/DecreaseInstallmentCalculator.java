package com.witoldw.calcs.imp;

import com.witoldw.calcs.Credit;
import com.witoldw.calcs.InstallmentCalculator;

public class DecreaseInstallmentCalculator extends Credit implements InstallmentCalculator {

	public DecreaseInstallmentCalculator(double capital, int issueOfInstallment, double rateOfInterest, double standingCharge) {
		super(capital, issueOfInstallment, rateOfInterest, standingCharge);
	}

	@Override
	public double calculateInterest() {
		return Math.round( ( (calculateCapitalInstallment() * (1 + (this.issueOfInstallment - this.getInstallmentNumber() + 1) * (this.getRateOfInterest() / 100.0)/12.0 )) - calculateCapitalInstallment()) *100.0)/100.0;
 	}

	@Override
	public double calculateTotalAmountOfInstallments() {		
		return calculateCapitalInstallment() + calculateInterest() + super.getStandingCharge();
	}
	
	@Override
	public double calculateCapitalInstallment(){
		return Math.round(  (super.getCapital() / (double) super.getIssueOfInstallment()) *100.0)/100.0;
	}
	
	public void setIssueOfInstallment(int issueOfInstallment){
		super.setIssueOfInstallment(issueOfInstallment);
	}

}
