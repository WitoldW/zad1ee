package com.witoldw.generators;

import java.io.IOException;

public interface Generator {
	
	void generateView() throws IOException ;
	void generatePDF();

}
