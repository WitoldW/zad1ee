package com.witoldw.generators.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.witoldw.calcs.imp.DecreaseInstallmentCalculator;
import com.witoldw.generators.Generator;

public class DecreaseInstallmentGenerator implements Generator {

	DecreaseInstallmentCalculator decreaseInstallmentCalculator;
	HttpServletResponse response;

	public DecreaseInstallmentGenerator(HttpServletRequest request, HttpServletResponse response) {
		decreaseInstallmentCalculator = new DecreaseInstallmentCalculator(
				Double.parseDouble(request.getParameter("kwota")), Integer.parseInt(request.getParameter("liczbaRat")),
				Double.parseDouble(request.getParameter("oprocentowanie")),
				Double.parseDouble(request.getParameter("oplataStala")));
		this.response = response;
	}

	@Override
	public void generateView() throws IOException {
		response.getWriter().println("<h1> Harmonogram spłat </h1>");
		response.getWriter().println(
				"<table cellpadding='10' ><tr><th>Nr raty</th><th>Kwota kapitału</th><th>Kwota odsetek</th><th>Opłata stała</th><th>Całkowita kwota raty</th></tr>");
		for (int i = 0; i < decreaseInstallmentCalculator.getIssueOfInstallment(); i++) {
			decreaseInstallmentCalculator.setInstallmentNumber(i + 1);
			response.getWriter()
					.println("<tr><td>" + (i + 1) + "</td><td>"
							+ decreaseInstallmentCalculator.calculateCapitalInstallment() + " zł</td><td>"
							+ decreaseInstallmentCalculator.calculateInterest() + " zł</td><td>"
							+ decreaseInstallmentCalculator.getStandingCharge() + " zł</td><td>"
							+ decreaseInstallmentCalculator.calculateTotalAmountOfInstallments() + " zł</td>");
		}
		response.getWriter().println("</table>");
	}

	@Override
	public void generatePDF() {		
		Document document = new Document();

		try {
			response.setContentType("application/pdf");
			PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
			BaseFont helvetica = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
			Font font = new Font(helvetica, 25);
			document.add(new Paragraph("Harmonogram spłat", font));
			document.add(new Paragraph(" "));
			PdfPTable table = new PdfPTable(5);
			table.setWidthPercentage(100);
			table.setWidths(new float[] { 10f, 20f, 20f, 20f, 25f });
			font = new Font(helvetica, 12);
			table.addCell(new Phrase("Nr raty", font));
			table.addCell(new Phrase("Kwota kapitału", font));
			table.addCell(new Phrase("Kwota odsetek", font));
			table.addCell(new Phrase("Opłata stała", font));
			table.addCell(new Phrase("Całkowita kwota raty", font));

			for (int i = 0; i < decreaseInstallmentCalculator.getIssueOfInstallment(); i++) {
				decreaseInstallmentCalculator.setInstallmentNumber(i + 1);
				table.addCell((i + 1) + "");
				table.addCell(new Phrase(decreaseInstallmentCalculator.calculateCapitalInstallment() + " zł", font));
				table.addCell(new Phrase(decreaseInstallmentCalculator.calculateInterest() + " zł", font));
				table.addCell(new Phrase(decreaseInstallmentCalculator.getStandingCharge() + " zł", font));
				table.addCell(new Phrase(decreaseInstallmentCalculator.calculateTotalAmountOfInstallments() + " zł", font));
			}

			document.add(table);
		} catch (Exception e) {
			e.printStackTrace();
		}
		document.close();		
	}

}
