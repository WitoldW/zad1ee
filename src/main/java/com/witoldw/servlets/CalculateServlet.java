package com.witoldw.servlets;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.witoldw.generators.Generator;
import com.witoldw.generators.impl.ConstantInstallmentGenerator;
import com.witoldw.generators.impl.DecreaseInstallmentGenerator;

@WebServlet("/calculate")
public class CalculateServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Generator generator;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html;  charset=UTF-8");

		if (request.getParameter("action") == null || request.getParameter("action").equals("")
				|| request.getParameter("kwota") == null || request.getParameter("kwota").equals("")
				|| request.getParameter("liczbaRat") == null || request.getParameter("liczbaRat").equals("")
				|| request.getParameter("oprocentowanie") == null || request.getParameter("oprocentowanie").equals("")
				|| request.getParameter("oplataStala") == null || request.getParameter("oplataStala").equals("")
				|| request.getParameter("rodzajRat") == null || request.getParameter("rodzajRat").equals("")) {
			response.sendRedirect("/index.jsp");
		} else {

			if (request.getParameter("rodzajRat").equals("stala")) {
				generator = new ConstantInstallmentGenerator(request, response);
			} else if (request.getParameter("rodzajRat").equals("malejaca")) {
				generator = new DecreaseInstallmentGenerator(request, response);
			}

			if (request.getParameter("action").equals("Oblicz!")) {
				generator.generateView();
			} else if (request.getParameter("action").equals("Generuj PDF!")) {
				generator.generatePDF();
			}

		}

	}
}
