package com.witoldw.tests;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.witoldw.calcs.imp.DecreaseInstallmentCalculator;

import org.junit.After;
import org.junit.Assert; 

public class TestDecreaseInstallmentCalculator {
	
	@Mock
	private DecreaseInstallmentCalculator decCalc;
	
	@Before
	public void setUp(){
		decCalc = new DecreaseInstallmentCalculator(10000.0,10,10.0,150.0);
	}
	
	@After
	public void clear(){
		decCalc = null;
	}
	
	@Test
	public void testCalculateInterest(){		
		Assert.assertEquals(91.67,decCalc.calculateInterest(),0.1 );
	}

	@Test
	public void testCalculateCapitalInstallment(){
		Assert.assertEquals(1000.0, decCalc.calculateCapitalInstallment(), 0.1);
	}
	
	@Test
	public void testCalculateTotalAmount(){
		Assert.assertEquals(1241.67, decCalc.calculateTotalAmountOfInstallments(), 0.1);
	}
	
	
}
