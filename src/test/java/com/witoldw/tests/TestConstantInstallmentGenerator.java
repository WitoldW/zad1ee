package com.witoldw.tests;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import com.witoldw.generators.impl.ConstantInstallmentGenerator;

public class TestConstantInstallmentGenerator extends Mockito{

	@Test
	public void testGenerateView() throws IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		when(request.getParameter("kwota")).thenReturn("1000");
		when(request.getParameter("oprocentowanie")).thenReturn("10");
		when(request.getParameter("oplataStala")).thenReturn("10");
		when(request.getParameter("liczbaRat")).thenReturn("10");
		
		new ConstantInstallmentGenerator(request, response).generateView();
		
		verify(writer).println("<h1> Harmonogram spłat </h1>");
		verify(writer).println(
				"<table cellpadding=\'10\' ><tr><th>Nr raty</th><th>Kwota kapitału</th><th>Kwota odsetek</th><th>Opłata stała</th><th>Całkowita kwota raty</th></tr>");
		verify(writer).println("<tr><td>1</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>2</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>3</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>4</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>5</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>6</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>7</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>8</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>9</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("<tr><td>10</td><td>100.0 zł</td><td>10.0 zł</td><td>10.0 zł</td><td>120.0 zł</td>");
		verify(writer).println("</table>");
	}
	
}
