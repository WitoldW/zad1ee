package com.witoldw.tests;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.witoldw.calcs.imp.ConstantInstallmentCalculator;

import org.junit.After;
import org.junit.Assert; 

public class TestConstantInstallmentCalculator {
	
	@Mock
	private ConstantInstallmentCalculator constantCalc;
	
	@Before
	public void setUp(){
		constantCalc = new ConstantInstallmentCalculator(10000.0,10,10.0,150.0);
	}
	
	@After
	public void clear(){
		constantCalc = null;
	}
	
	@Test
	public void testCalculateInterest(){		
		Assert.assertEquals(100.0,constantCalc.calculateInterest(),0.1 );
	}

	@Test
	public void testCalculateCapitalInstallment(){
		Assert.assertEquals(1000.0, constantCalc.calculateCapitalInstallment(), 0.1);
	}
	
	@Test
	public void testCalculateTotalAmount(){
		Assert.assertEquals(1250.0, constantCalc.calculateTotalAmountOfInstallments(), 0.1);
	}
	
	
}
